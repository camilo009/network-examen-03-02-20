from django.contrib import admin
from django.urls import path,include
from . import views
from rest_framework import routers
from .viewsets import UsuarioViewSet ,RutinaViewSet,NutricionistaViewSet
app_name = 'API'
router = routers.DefaultRouter()
router.register(u'usuarios',UsuarioViewSet)
router.register(u'nutricionistas',NutricionistaViewSet)
router.register(u'rutinas',RutinaViewSet)
urlpatterns =  [ 

   path('',views.index,name="index"), 
   path('api',include(router.urls)),
   path('delete_mensaje/<int:id>/',views.delete_mensaje,name="delete_mensaje"),
   path('edit_mensaje/<int:id>/',views.edit_mensaje, name="edit_mensaje"),
   
]