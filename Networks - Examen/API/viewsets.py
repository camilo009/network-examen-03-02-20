from rest_framework import viewsets

from .serializers import NutricionistaSerializer,RutinaSerializer,UsuarioSerializer
from proyecto.models import Nutricionista,Usuario,Rutina
from rest_framework.authentication import SessionAuthentication ,BaseAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response



class NutricionistaViewSet(viewsets.ModelViewSet):
    queryset = Nutricionista.objects.all()
    serializer_class = NutricionistaSerializer



class RutinaViewSet(viewsets.ModelViewSet):
    queryset = Rutina.objects.all()
    serializer_class = RutinaSerializer


class UsuarioViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer
