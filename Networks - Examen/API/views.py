from django.shortcuts import render,HttpResponse,HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import Mensaje
# Create your views here.



#--------------------------------------------CREAR  Rutina---------------------------------------------
# crear
def index(request):
    if request.POST:
        print("post")
        usuario =request.POST.get("usuario",False)
        rutina=request.POST.get("rutina",False)
        mensaje = request.POST.get("mensaje",False)
        foto = request.FILES.get("foto",False)
        if foto is False:
            foto = None
        msn = Mensaje(usuario=usuario,rutina=rutina,mensaje=mensaje,foto=foto)
        msn.save()
        messages.add_message(request, messages.SUCCESS,'Creado con exito')
    mensajes = Mensaje.objects.all()        
    context= {'mensajes':mensajes}
    return render(request,"index.html",context)
    

#eliminar#
def delete_mensaje(request,id):
    try:
        msn = Mensaje.objects.get(id=id)
        msn.delete()
        messages.add_message(request, messages.SUCCESS, 'Borrado con éxito')
    except Mensaje.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'No encontrado')

    return HttpResponseRedirect(reverse("index"))    

 #ACTUALIZAR
def edit_mensaje(request,id):
    try:
        msn = Mensaje.objects.get(id=id)
        mensajes = Mensaje.objects.all()
        context ={'mensajes':mensajes,'mensaje':msn}
        if request.POST:
            usuario =request.POST.get("usuario",False)
            rutina=request.POST.get("rutina",False)
            mensaje = request.POST.get("mensaje",False)
            foto = request.FILES.get("foto",False)
            borrar_foto =request.POST.get("borrar_foto",False)
            msn.usuario = usuario
            msn.rutina = rutina
            msn.mensaje = mensaje
            if foto:
                msn.foto = foto
            if borrar_foto:
                msn.foto = None
            msn.save()
            messages.add_message(request, messages.SUCCESS, 'Editado con éxito')
            return HttpResponseRedirect(reverse("index"))
        return render(request,"index.html",context)
    except Mensaje.DoesNotExist:
        messages.add_message(request, messages.ERROR, 'No encontrado')
    return HttpResponseRedirect(reverse("index"))


# Buscar #

