from rest_framework import serializers
from proyecto.models import Nutricionista,Rutina,Usuario

class NutricionistaSerializer(serializers.ModelSerializer):

     class Meta:

          model = Nutricionista
          fields = ['nombreNutricionista', 'idNutricionista']

class RutinaSerializer(serializers.ModelSerializer):

     class Meta:

          model =Rutina
          fields = ['nombreRutina', 'idRutina']          


class UsuarioSerializer(serializers.ModelSerializer):

     class Meta:

          model = Usuario
          fields = ['username', 'last_name','first_name']          