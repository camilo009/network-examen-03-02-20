from django.contrib import admin
from .models import Usuario,Rutina,Nutricionista
# Register your models here.

admin.site.register(Usuario)
admin.site.register(Nutricionista)
admin.site.register(Rutina)