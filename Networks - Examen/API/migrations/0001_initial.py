# Generated by Django 3.0.2 on 2020-01-28 16:51

import API.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Mensaje',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.CharField(max_length=100)),
                ('rutina', models.CharField(max_length=25)),
                ('mensaje', models.TextField()),
                ('foto', models.ImageField(upload_to=API.models.PathAndRename('fotos'))),
            ],
        ),
    ]
