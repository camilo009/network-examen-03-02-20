from django.db import models
from django.utils.deconstruct import deconstructible
import os
from uuid import uuid4

# Create your models here.


@deconstructible
class PathAndRename(object):
    def __init__(self,sub_path):
        self.path =sub_path

    def __call__(self, instance, filename):
         ext = filename.split('.')[-1]
         filename = '{}.{}'.format(uuid4().hex, ext)
         return os.path.join(self.path, filename)   

path_and_rename_imagen = PathAndRename("fotos")


class Mensaje(models.Model):

     usuario =models.CharField(max_length=100)
     rutina =models.CharField(max_length=25)
     mensaje =models.TextField()
     foto =models.ImageField(upload_to=path_and_rename_imagen)



##---------------Rutina ----------------------

#djang agrega id 
class Rutina(models.Model):

    nombreRutina = models.CharField(max_length=50)

def __str__(self):
  return self.nombreRutina

#-------------------USUARIO#---------------------

class Usuario(models.Model):

  username =   models.CharField(max_length=50)
  first_name = models.CharField(max_length=50)
  last_name =  models.CharField(max_length=50)
  email =      models.CharField(max_length=50)
  password = models.CharField(max_length=50)
  rut =    models.CharField(max_length=30, unique=True)

  def __str__(self):
    return self.first_name
# ------------------Nutricionista-------------------#

class Nutricionista(models.Model):

  nombreNutricionista = models.CharField(max_length=50)