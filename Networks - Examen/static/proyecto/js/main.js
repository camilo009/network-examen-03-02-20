//para asegurar que el service worker sea compatible 
if('serviceWorker' in navigator){
  window.addEventListener('load', ()=>{
      navigator.serviceWorker
      .register('../servicesworker.js')
      .then(reg => console.log('Service Worker :Registered'))
      .catch(err => console.log('Service Worker:error: ${err}'))
  })
}