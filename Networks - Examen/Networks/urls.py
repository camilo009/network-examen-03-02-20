
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path,include
from django.contrib.auth.decorators import login_required
from proyecto.views import Inicio,cerrar_sesion,Rutinas,ListadoUsuariosView
from proyecto.models import Usuario
from API import views


app_name='Networks'
urlpatterns = [
    path('admin/', admin.site.urls),
    path('proyecto/',include(('proyecto.urls_proyecto','proyecto'))),
    path('api/',include(('API.urls'))),
    path('',Inicio.as_view(),name='inicio'),
    path('rutinas/',login_required(Rutinas.as_view()),name='rutinas'),
    path('login/',auth_views.LoginView.as_view(template_name="proyecto/login.html"),name='login'),
    path('logout/',cerrar_sesion,name='logout'),
    path('password-reset/',auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'),name='password_reset'),
    path('password-reset/done/',auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'),name='password_reset_done'),
    path('password-reset-complete/',auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'),name='password_reset_complete'),
    path('password-reset-confirm/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html')),
    path('listar/',ListadoUsuariosView.as_view(template_name='proyecto/listarUsuarios.html'), name='listarUsuario'),
    path('index/',views.index,name='index'),

  

]