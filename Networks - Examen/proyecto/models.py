from django.db import models
from django.forms import ModelForm
from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Usuario(AbstractUser):
    rut=models.CharField(max_length=9, blank= False, null= False)
    usuario=models.CharField(max_length=50, blank= False, null= False)
    email=models.CharField(max_length=50, blank= False, null= False)
    password=models.CharField(max_length=25, blank= False, null= False)
    creado = models.DateTimeField(auto_now=True)
    foto = models.ImageField(upload_to="fotos",default="default.png")

    class Meta:
        verbose_name='Usuario'
        verbose_name_plural='Usuarios'


    def __str__(self):
        return self.username


class Rutina(models.Model):
    idRutina=models.CharField(max_length=7, primary_key=True,blank=False, null=False)
    nombreRutina=models.CharField(max_length=25,blank=False, null=False)

    class Meta:
        verbose_name='Rutina'
        verbose_name_plural='Rutinas'

    def __str__(self):
        return self.nombreRutina

class Nutricionista(models.Model):
    idNutricionista=models.CharField(max_length=7,primary_key=True, blank=False, null=False)
    nombreNutricionista=models.CharField(max_length=25, blank=False,null=False)
    creado = models.DateTimeField(auto_now=True)
    foto = models.ImageField(upload_to="fotos",default="default.png")


    class Meta:
        verbose_name='Nutricionista'
        verbose_name_plural='Nutricionistas'

    def __str__(self):
        return self.nombreNutricionista
