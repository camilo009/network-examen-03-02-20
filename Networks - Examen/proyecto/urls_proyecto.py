from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import nutricionista_create,registrar,cerrar_sesion,listarUsuario,ListarNutricionista,ListadoRutina,EliminarNutricionista,ActualizarNutricionista,CrearNutricionista,EliminarRutina,ActualizarRutina,CrearRutina,inicioUsuarios,ListadoUsuarios,eliminarUsuario,EliminarUsuario,Contacto,Mision,Quienes_Somos
from . import views
from django.contrib.auth import login,logout
from django.conf.urls import url



app_name='proyecto'
urlpatterns=[

    path('login/',views.login,name="login"),
    path('listarUsuarios/',ListadoUsuarios.as_view(),name="listarUsuarios"),
    path('eliminarUsuarios/<slug:pk>',EliminarUsuario.as_view(),name="eliminarUsuario"),
    path('contacto/',Contacto.as_view(),name="contacto"),
    path('mision/',Mision.as_view(),name="mision"),
    path('quienes_somos/',Quienes_Somos.as_view(),name="quienes_somos"),
    path('inicioUsuarios/',inicioUsuarios.as_view(),name="inicioUsuarios"),
    path('listarRutinas/',ListadoRutina,name="listarRutinas"),
    path('agregarRutina/',CrearRutina.as_view(),name="agregarRutina"),
    path('editarRutina/<slug:pk>',ActualizarRutina.as_view(),name="editarRutina"),
    path('eliminarRutina/<slug:pk>',EliminarRutina.as_view(),name="eliminarRutina"),
    path('listarNutricionista/',ListarNutricionista,name="listarNutricionista"),
    path('agregarNutricionista/',CrearNutricionista.as_view(),name="agregarNutricionista"),
    path('editarNutricionista/<slug:pk>',ActualizarNutricionista.as_view(),name="editarNutricionista"),
    path('eliminarNutricionista/<slug:pk>',EliminarNutricionista.as_view(),name="eliminarNutricionista"),
    path('logout/',cerrar_sesion, name='logout'),
    path('registro/',registrar,name='registro'),
    path('borrar/<int:id>',views.borrar,name="borrar"),
    path('actualizar/<int:id>',views.actualizar,name="actualizar"),
    path('nutricionista/',views.nutricionista_create,name="nutricionista"),
   


] 
