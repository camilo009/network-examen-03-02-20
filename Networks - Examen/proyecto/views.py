
from django.shortcuts import render,redirect
from django.core.exceptions import ObjectDoesNotExist
from .models import Usuario,Rutina,Nutricionista
from django.views.generic import TemplateView,ListView,UpdateView,CreateView,DeleteView
from django.urls import reverse_lazy,reverse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone
from django.views.generic.list import ListView
from proyecto.models import  Usuario
from proyecto.forms import FormularioLogin,RutinaForm,NutricionistaForm, FormularioLogin
from . import config
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.views.generic.base import View
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required




 


def registrar(request):
    if request.POST:
        rut = request.POST.get('rut',False)
        usuario = request.POST.get('usuario',False)
        apellido = request.POST.get('apellido',False)
        nombre = request.POST.get('nombre',False)
        print("USUARIO",usuario)
        email = request.POST.get('email',False)
        password = request.POST.get('password',False)
        u = Usuario(rut=rut,usuario=usuario,password=password,email=email,username=usuario,first_name=nombre,last_name=apellido)
        u.set_password(password)
        u.save()
        return HttpResponseRedirect(reverse('inicio'))
    return render(request, 'proyecto/registroUsuarios.html')





# Create your views here.
"""
def RegistroUsuario(request):
    if request.POST:
        print("post")
        rut =request.POST.get("rut",False)
        username=request.POST.get("username",False)
        email = request.POST.get("email",False)
        password = request.POST.get("password",False)
    
        ru = Usuario(rut=rut,username=username,email=email,password=password)
        ru.save()
        messages.add_message(request, messages.SUCCESS,'Usuario Creado')
           
    context= {'usuario':Usuario}
    return render(request,'proyecto/RegistroUsuarios.html',context)
"""
class ListadoUsuariosView(ListView):
    model = Usuario
    paginate_by = 100
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


def iniciar_sesion(request):
    if request.POST:
        usuario = request.POST.get('usuario',False)
        contrasenia = request.POST.get('contrasenia',False)
        
        if usuario and contrasenia:
            user = authenticate(request=request,username=usuario,password=contrasenia)
            if user is not None:
                login(request,user)
            else:
                messages.add_message(request, messages.INFO, 'Usuario incorrecto')
        else:
            messages.add_message(request, messages.INFO, 'Debes ingresar los datos')

    return render(request,'login.html')

def cerrar_sesion(request):
    logout(request)
    return HttpResponseRedirect(reverse('login'))
  
  

#-------------------------------LINKS DE LA PAGINAS SIMPLES-----------------------------
class Inicio(TemplateView):
    template_name='inicio.html'
class Rutinas(TemplateView):
    template_name='proyecto/Rutinas.html'
class Contacto(TemplateView):
    template_name= 'proyecto/contacto.html'
class Mision (TemplateView):
    template_name= 'proyecto/mision.html'
class Quienes_Somos(TemplateView):
    template_name= 'proyecto/quienes_somos.html'
class inicioUsuarios(TemplateView):
    template_name='proyecto/login.html'



#----------------------------LISTADO DE DATOS-----------------------------------------


class ListadoUsuarios(ListView):
    model = Usuario
    Template_name ='proyecto/listarUsuarios.html'
    context_object_name='Usuario'
    queryset=Usuario.objects.all()


class ListarNutricionista(ListView):
    model=Nutricionista
    template_name='proyecto/listarNutricionista.html'
    context_object_name='nutricionista'
    queryset=Nutricionista.objects.all()




def listarUsuario(request):
    usuarios=Usuario.objects.all()
    return render(request,'proyecto/listarUsuarios.html',{'usuarios:listarUsuario'})



def ListadoRutina(request):
    Rutina=Rutina.objects.all()
    return render(request,'proyecto/agregarRutina.html',{'listadoRutina':Rutina})



def listarNutricionista(request):
    Nutricionista=Nutricionista.objects.all()
    return render(request,'proyecto/listarNutricionista',{'nutricionista':Nutricionista})


#-------------------------------CREACION DE DATOS------------------------------------





class CrearRutina(CreateView):
    model=Rutina
    form_class=RutinaForm
    template_name='proyecto/agregarRutina.html'
    success_url=reverse_lazy('proyecto:rutinas')

class CrearNutricionista(CreateView):
    model=Nutricionista
    form_class=NutricionistaForm
    template_name='proyecto/agregarNutricionista.html'
    success_url=reverse_lazy('proyecto:listarNutricionista')

   #---------------------------------------------------------------------- 







def RegistroRutina(request):
    if request.method=='POST':
        Rutina_form=RutinaForm(request.POST)
        if Rutina_form.is_valid():
            Rutina_form.save()
        else:
            Rutina_form=RutinaForm()
        return render(request,'proyecto/agregarRutina.html',{'rutina_form':RutinaForm})


#--------------------------------ACTUALIZACION DE DATOS--------------------------------------

"""
class ActualizarUsuarios(UpdateView):
    model=Usuario
    form_class=UsuarioForm
    template_name='proyecto/editarUsuario.html'
    success_url=reverse_lazy('proyecto:listarUsuarios')
"""
class ActualizarRutina(UpdateView):
    model=Rutina
    template_name='proyecto/agregarRutina.html'
    form_class=RutinaForm
    success_url=reverse_lazy('proyecto:listarRutina')

class ActualizarNutricionista(UpdateView):
    model=Nutricionista
    template_name='proyecto/agregarNutricionista'
    form_class=NutricionistaForm
    success_url=reverse_lazy('proyecto:listarNutricionista')



class EliminarUsuario(DeleteView):
    model=Usuario
    success_url=reverse_lazy('proyecto:listarUsuarios')


def eliminarUsuario(request,Rut):
        usuario=Usuario.objects.get(Rut=Rut)
        if request.method=='POST':
            usuario.delete()
            return redirect('listarUsuarios')
        return render(request,'proyecto/eliminarUsuarios.html',{'usuario':usuario})




class EliminarRutina(DeleteView):
    model=Rutina
    success_url=reverse_lazy('proyecto:listarRutina')


class EliminarNutricionista(DeleteView):
    model=Nutricionista
    success_url=reverse_lazy('proyecto:listarNutricionista')





#-----------------------Subir fotos usuario y nutricionista pero ahora en el localhost#

def crear(request):


    if request.POST:
        Usuario =request.POST.get('usuario','')
        first_name = request.POST.get('first name')
        rut = request.POST.get('rut','')
        email = request.POST.get('email','')
        tipo = request.POST.get('tipo')
        foto = request.FILES.get('foto',False)
        username =request.POST.get('username','')
        usuario = Usuario(usuario=usuario,first_name=first_name,email=email,tipo=Nutricionista.objects.get(pk=tipo))
    if foto:
         usuario.foto = foto
         messages.add_message(request, messages.INFO,'Usuario creado')

         context ={
             'lista_tda' :   Nutricionista.objects.all(),
             'lista_usuario': Usuario.objects.all()
         }
         return render(request,'index.html',context)


@login_required(login_url="/login")
def borrar(request,rut):
       usuario = Usuario.objects.get(pk=rut)
       usuario.delete()
       messages.add_message(request, messages.INFO,'el usuario fue borrado')
       return HttpResponseRedirect(reverse('nutricionista'))

@staff_member_required(login_url="/login")
def actualizar(request,rut):
    usuario = Usuario.objects.get(pk=rut)
    if request.POST:
      usuario.username = request.POST.get('username','')
      usuario.first_name = request.POST.get('first name','')
      usuario.rut = request.POST.get('rut','')
      usuario.email = request.POST.get('email','')
      usuario.password = request.POST.get('password','')
      usuario.save()
      messages.add_message(request, messages.INFO,'{} fue editado'.format(usuario.username))
      return HttpResponseRedirect(reverse('nutricionista'))
    context = {
       'lista_tda' : Nutricionista.objecta.all(),
       'usuario':usuario
   }
    return render(request,'actualizar_cosas.html',context)


def nutricionista_create(request):
    if request.POST:
        form = NutricionistaForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO,'nutricionista fue creada') 
            return HttpResponseRedirect(reverse('nutricionista'))
    else:
        form = NutricionistaForm()
    return render(request,'proyecto/Rutinas.html',{'form':form})  
              