from django import forms
from django.contrib.auth.forms import AuthenticationForm,UserCreationForm
from .models import Usuario,Rutina,Nutricionista
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Extendemos del original
class RegistrarUsuario(forms.ModelForm):
    class Meta():
        model = Usuario
        help_texts ={'usuario':None}
        fields = ('rut','usuario','password','email')
        labels = {'rut':('rut'),'usuario':('usuario'),'password':('password'),'email':('correo')}


class FormularioLogin(AuthenticationForm):
    def __init__(self,*args,**kwargs):
        super(FormularioLogin,self).__init__(*args,**kwargs)
        self.fields['username'].widget.attrs['class']='form-control'
        self.fields['username'].widget.attrs['placeholder']='Nombre de Usuario'
        self.fields['password'].widget.attrs['class']='form-control'
        self.fields['password'].widget.attrs['placeholder']='Contraseña'


class RutinaForm(forms.ModelForm):
    class Meta:
        model=Rutina
        fields=['idRutina','nombreRutina']
        labels={
            'Idrutina': 'Id del Rutina',
            'nombreRutina': 'Nombre del Rutina'
        }
        widgets={
            'idRutina': forms.TextInput(
                attrs={
                    'class':'input',
                    'placeholder':'Ingrese Id del Rutina'
                }
            ),
            'nombreRutina': forms.TextInput(
                attrs={
                    'class':'input',
                    'placeholder':'Ingrese nombre Rutina'
                }
            )
        }

class NutricionistaForm(forms.ModelForm):
    class Meta:
        model=Nutricionista
        fields=['idNutricionista','nombreNutricionista']
        labels={
            'idNutricionista': 'Id del la Nutricionista',
            'nombreNutricionista': 'Nombre del la Nutricionista'
        }
        widgets={
            'idNutricionista': forms.TextInput(
                attrs={
                    'class':'input',
                    'placeholder':'Ingrese Id de la Nutricionista'
                }
            ),
            'nombreNutricinista': forms.TextInput(
                attrs={
                    'class':'input',
                    'placeholder':'Ingrese nombre de la Nutricionista'
                }
            )
        }
  